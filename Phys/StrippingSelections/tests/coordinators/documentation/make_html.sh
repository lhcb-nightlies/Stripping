#!/bin/sh

export TCK='0x41412800'
export STRIP='stripping28'

python utils_new.py ${TCK}
rm -f config.tar.lock
python utils_new.py ${TCK} ${STRIP}
rm -f config.tar.lock
