#!/bin/bash

dvversion=v42r5
stepid=131451
datatype=2017
opts=$PWD/Stripping-S29.py

./run \$STRIPPINGSELECTIONSROOT/tests/StrippingTest.py --datafile \$STRIPPINGSELECTIONSROOT/tests/data/Reco16_Run182594.py --genpydoc --pydocdvver $dvversion --DataType $datatype --pydocstepid $stepid --EvtMax 0 --importoptions $opts --HLT2Rate 0.