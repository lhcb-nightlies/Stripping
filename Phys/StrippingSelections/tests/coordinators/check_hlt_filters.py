#!/bin/env python

'''Check the HLT filters of the stripping lines against the list of HLT lines
for a given stripping version and HLT1 & HLT2 tcks.'''

import sys, os
sys.path.insert(0, os.path.join(os.environ['STRIPPINGSELECTIONSROOT'], 'tests', 'python'))
from StrippingTests.HLT import check_hlt_filters
from StrippingTests.Utils import build_streams, stripping_args
from pprint import pformat

def main() :
    argparser = stripping_args()
    argparser.add_argument('--hlt1tck', help = 'TCK for HLT1 or both HLT1 & HLT2 lines')
    argparser.add_argument('--hlt2tck', default = None,
                           help = 'TCK for HLT2 lines. If None, the HLT1 tck is used for both HLT1 & HLT2')
    argparser.add_argument('--outputfile', default = None,
                           help = '''Name of file to which to write the dict of lines without HLT matches.
If None, the dict is output to the console.''')

    args = argparser.parse_args()
    if args.stripping :
        streams = build_streams(args.stripping, args.stripping, usedb = (not args.nodb))
    else :
        streams = build_streams(args.settings, args.archive, usedb = (not args.nodb))

    nomatchlines = check_hlt_filters(streams, args.hlt1tck, args.hlt2tck)

    if args.outputfile :
        with open(args.outputfile, 'w') as f :
            f.write(pformat(nomatchlines))
    else :
        print pformat(nomatchlines)

if __name__ == '__main__' :
    main()
