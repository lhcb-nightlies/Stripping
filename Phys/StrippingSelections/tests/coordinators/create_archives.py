#!/bin/env python

'''Create/update the StrippingSettings, StrippingArchive, and CommonParticlesArchive
archives for a given stripping version.'''

#from StrippingSettings.Utils import create_settings_archive
from StrippingSettings.Utils import generate_dbase, dbase, config_from_modules, strippingConfiguration
from StrippingArchive.Utils import create_stripping_archive
from CommonParticlesArchive import create_common_particles_archive
import sys, subprocess, os

try :
    stripping = sys.argv[1]
    helpstr = sys.argv[2]
except :
    print 'Usage: ./create_archives.py strippingversion "Short description of stripping version"'
    sys.exit(0)

#create_settings_archive(stripping) # This isn't quite ready yet.
# Only regenerate the dbase if necessary
updateconfig = True
if os.path.exists(dbase(stripping)) :
    checkconfig = dict(strippingConfiguration(stripping))
    newconfig = config_from_modules(stripping)
    updateconfig = (newconfig != checkconfig)
if updateconfig :
    generate_dbase(stripping)
    subprocess.call('mv stripping.tmp $STRIPPINGSETTINGSROOT/dbase/' + stripping.lower(), shell = True)

create_stripping_archive(stripping, helpstr)
create_common_particles_archive(stripping)
