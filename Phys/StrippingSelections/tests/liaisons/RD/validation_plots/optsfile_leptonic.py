#author: Guido Andreassi <guido.andreassi@cern.ch>
#This is an optionsfile for producing the ntuples needed for the validation of the stripping (Leptonic stream) in RD

from DaVinci.Configuration import *
from Gaudi.Configuration import *
from DecayTreeTuple.Configuration import *

JpsieeK = DecayTreeTuple('JpsieeK')   
JpsieeK.Inputs = ["/Event/Leptonic/Phys/LFVBu2KJPsieeLine/Particles"]
JpsieeK.Decay = "[B+ -> ^(J/psi(1S) -> ^e+ ^e-) ^K+]CC"

JpsimmK = DecayTreeTuple('JpsimmK')   
JpsimmK.Inputs = ["/Event/Leptonic/Phys/Bs2MuMuLinesBu2JPsiKLine/Particles"]
JpsimmK.Decay = "[B+ -> ^(J/psi(1S) -> ^mu+ ^mu-) ^K+]CC"

#B2rhogamma = DecayTreeTuple('B2rhogamma')
#B2rhogamma.Inputs = ["/Event/Leptonic/Phys/Beauty2XGamma2pi_Line/Particles"]
#B2rhogamma.Decay = "B0 -> ^(rho(770)0 -> ^pi+ ^pi-) ^gamma"

Lb2JpsieeL = DecayTreeTuple('Lb2JpsieeL')
Lb2JpsieeL.Inputs = ["/Event/Leptonic/Phys/Bu2LLK_eeLine/Particles"]
Lb2JpsieeL.Decay = "[Lambda_b0 -> ^(J/psi(1S) -> ^e+ ^e-) ^Lambda0]CC"

Lb2JpsimmL = DecayTreeTuple('Lb2JpsimmL')
Lb2JpsimmL.Inputs = ["/Event/Leptonic/Phys/Bu2LLK_mmLine/Particles"]
Lb2JpsimmL.Decay = "[Lambda_b0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^Lambda0]CC"

Ups2ee = DecayTreeTuple('Ups2ee')
Ups2ee.Inputs = ["/Event/Leptonic/Phys/LFVupsilon2eeLine/Particles"]
Ups2ee.Decay = "Upsilon(1S) -> ^e+ ^e-"

DaVinci().MoniSequence += [JpsieeK, JpsimmK, B2rhogamma, Lb2JpsieeL, Lb2JpsimmL, Ups2ee]
DaVinci().TupleFile = "S29_validation_leptonic.root"
#DaVinci().EventPreFilters += [stripFilter]
DaVinci().DDDBtag   = 'dddb-20150724' 
DaVinci().CondDBtag = 'cond-20170510'
#DaVinci().DQFLAGStag    = dqflag
DaVinci().InputType = 'MDST'
DaVinci().DataType = '2017' 

#for local test
""" 
import os
from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles(
        ['./00063876_00000019_1.leptonic.mdst',
         './00063876_00000029_1.leptonic.mdst']
        , clear=True)
"""
