# lb-run LHCbDirac/prod dirac-bookkeeping-get-files Reco16_pHe.py --Runs 186400 --BKQuery /LHCb/Protonhelium16/Beam4000GeV-VeloClosed-MagDown/RealData/Reco16Smog/90300000/RDST

from Gaudi.Configuration import *
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000489_1.rdst',
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000490_1.rdst',
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000491_1.rdst',
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000492_1.rdst',
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000493_1.rdst',
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000494_1.rdst',
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000495_1.rdst',
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000496_1.rdst',
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000497_1.rdst',
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000499_1.rdst',
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000500_1.rdst',
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000501_1.rdst',
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000502_1.rdst',
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000503_1.rdst',
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000504_1.rdst',
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000505_1.rdst',
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000506_1.rdst',
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000507_1.rdst',
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000508_1.rdst',
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000509_1.rdst',
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000510_1.rdst',
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000511_1.rdst',
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000512_1.rdst',
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000513_1.rdst',
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000514_1.rdst',
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000515_1.rdst',
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000516_1.rdst',
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000517_1.rdst',
'LFN:/lhcb/LHCb/Protonhelium16/RDST/00055503/0000/00055503_00000518_1.rdst',
], clear=True)


from Gaudi.Configuration import FileCatalog

FileCatalog().Catalogs = [ 'xmlcatalog_file:$STRIPPINGSELECTIONSROOT/tests/data/Reco16_pHe.xml' ]

