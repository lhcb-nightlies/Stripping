'''Generic utilities for testing the Stripping.'''

import StrippingSettings.Utils, StrippingArchive.Utils, StrippingSelections.Utils
from argparse import ArgumentParser

def stripping_config(stripping, usedb = True) :
    if stripping :
        if usedb :
            return StrippingSettings.Utils.strippingConfiguration(stripping)
        else :
            return StrippingSettings.Utils.config_from_modules(stripping)
    return StrippingSelections.buildersConf()

def build_streams(strippingconfig, 
                  archive = None,
                  WGs = None,
                  usedb = True) :
    if None == strippingconfig or isinstance(strippingconfig, str) :
        strippingconfig = stripping_config(strippingconfig, usedb)
    if isinstance(archive, str) :
        archive = StrippingArchive.strippingArchive(archive)
    if archive :
        return StrippingArchive.Utils.buildStreams(strippingconfig, archive, WGs)
    return StrippingSelections.Utils.buildStreams(strippingconfig, WGs)

def stripping_name(stripping) :
    if stripping :
        return stripping[0].upper() + stripping[1:].lower()
    return stripping

def stripping_args(argparser = None) :
    if not argparser :
        argparser = ArgumentParser()
        argparser.add_argument('--settings', default = None, 
                           help = '''Version of config from StrippingSettings to use. 
Default None = default dicts from StrippingSelections.''')
    argparser.add_argument('--archive', default = None,
                           help = '''Version of lines from StrippingArchive to use.
Default None = default lines from StrippingSelections.''')
    argparser.add_argument('--stripping', default = None,
                           help = '''Version of lines & settings from StrippingSettings & 
StrippingArchive to use. Overrides --settings and --archive if not None.''')
    argparser.add_argument('--nodb', default = False, 
                           action = 'store_true', 
                           help = 'Use StrippingSettings from the modules and not the db.')
    return argparser
