#!/bin/env python

from Configurables import Stripping
from pprint import pformat

missinginputs = Stripping().check_input_output()
if missinginputs :
    raise Exception('Missing inputs:\n' + pformat(missinginputs))
