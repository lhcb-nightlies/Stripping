#!/usr/bin/env python

import os
import sys

'''
Generate a dbase module from a set of stripping line config
dictionaries.

Usage: 
  > python makeDB <Stripping> 
'''

if __name__ == '__main__' :
    from StrippingSettings.Utils import generate_dbase
    
    if 2 <= len(sys.argv):
        generate_dbase( sys.argv[1] )
    if 3 <= len(sys.argv) :
        os.system('mv stripping.tmp ' + sys.argv[2])
