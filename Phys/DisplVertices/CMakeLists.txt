################################################################################
# Package: DisplVertices
################################################################################
gaudi_subdir(DisplVertices v7r26)

gaudi_depends_on_subdirs(Det/VeloDet
                         Event/HltEvent
                         Phys/DaVinciKernel
                         Phys/LoKiPhys
                         Phys/LoKiTracks
                         Phys/LoKiArrayFunctors
                         Tr/TrackKernel
                         Phys/JetAccessories)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(DisplVertices
                 src/*.cpp
                 INCLUDE_DIRS AIDA
                 LINK_LIBRARIES VeloDetLib DaVinciKernelLib LoKiPhysLib
                                LoKiTracksLib LoKiArrayFunctorsLib TrackKernel
                                HltEvent)

gaudi_install_headers(Kernel)
gaudi_install_python_modules()

